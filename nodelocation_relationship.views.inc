<?php

/**
 *  Implementation of hook_views_data_alter().
 */

function nodelocation_relationship_views_data_alter(&$data) {
  $data['location']['nodelocation_ref_nid'] = array(
    'group' => t('Node location'),
    'title' => t('Reference'),
    'help' => t('Relate the location of a referenced node to the referring node.'),
    'real field' => 'nid',
    'relationship' => array(
      'handler' => 'nodelocation_relationship_handler_relationship_location',
      'base' => 'location',
      'field' => 'lid',
      'label' => t('Referenced lid'),
    ),
  );
}

/**
 *  Implementation of hook_views_handlers().
 */

function nodelocation_relationship_views_handlers() {  
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'nodelocation_relationship'),
    ),
    'handlers' => array(
      'nodelocation_relationship_handler_relationship_location' => array(
        'parent' => 'views_handler_relationship',
      ),
    ),
  );
}