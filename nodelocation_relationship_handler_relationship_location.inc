<?php

/**
 * Relationship handler to relate referenced node location to the referring node.
 */
class nodelocation_relationship_handler_relationship_location extends views_handler_relationship {
  function option_definition() {
    $options = parent::option_definition();

    $options['nodelocation_reference_field'] = array('default' => '');
    $options['nodelocation_reference_field_alias'] = array('default' => '');
    $options['nodelocation_reference_table'] = array('default' => '');

    return $options;
  }

  /**
   * Default options form that provides the label widget that all fields
   * should have.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    // compile an aray of all fields of type "nodereference"
    $all_fields = content_fields();
    $nodereference_fields = array();
    
    foreach ($all_fields as $field_key => $field_info) {
      if ($field_info['type'] == "nodereference") {
        $types = array();
        foreach (content_types() as $type) {
          if (isset($type['fields'][$field_info['field_name']])) {
            $types[] = $type['name'];
          }
        }
        $nodereference_fields[$field_info['field_name']] = $field_info['widget']['label'] . " (" . $field_info['field_name'] . "); appears in " . implode(', ', $types);
      }
    }
    
    $form['nodelocation_reference_field'] = array(
      '#type' => 'select',
      '#title' => t('Node reference field'),
      '#options' => $nodereference_fields,
      '#default_value' => $this->options['nodelocation_reference_field'],
    );
  }

  function query() {
    // create a relationship to join the cck field data for the node
    $join = new views_join();
    $join->definition = array(
      'left_table' => 'node',
      'left_field' => 'nid',
      'table' => $this->options['nodelocation_reference_table'],
      'field' => 'nid',
    );

    if (!empty($this->options['required'])) {
      $join->definition['type'] = 'INNER';
    }
    
    $join->construct();

    $alias = $join->definition['table'] . '_' . $join->definition['left_table'];
    $field_table_node_relationship = $this->query->add_relationship($alias, $join, $join->definition['table'], $this->relationship);
    
    // create a relationship to join the lid of the referenced node
    $join = new views_join();
    $join->definition = array(
      'left_table' => $field_table_node_relationship,
      'left_field' => $this->options['nodelocation_reference_field_alias'],
      'table' => 'location_instance',
      'field' => 'nid',
    );

    if (!empty($this->options['required'])) {
      $join->definition['type'] = 'INNER';
    }
    
    $join->construct();
    
    $alias = $join->definition['table'] . '_' . $field_table_node_relationship;
    $location_instance_field_table_relationship = $this->query->add_relationship($alias, $join, $join->definition['table'], $field_table_node_relationship);
    
    // join the locative information based on the lid
    $join = new views_join();
    $join->definition = array(
      'left_table' => $location_instance_field_table_relationship,
      'left_field' => 'lid',
      'table' => 'location',
      'field' => 'lid',
    );

    if (!empty($this->options['required'])) {
      $join->definition['type'] = 'INNER';
    }
    
    $join->construct();
    
    $alias = $join->definition['table'] . '_' . $location_instance_field_table_relationship;
    $this->alias = $this->query->add_relationship($alias, $join, $join->definition['table'], $location_instance_field_table_relationship);
  }
}